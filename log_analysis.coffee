###
Author: Wattana Inthaphong
Email: wattaint@gmail.com
###

fs    = require 'fs'
path  = require 'path'
es    = require 'event-stream'
numbers = require 'numbers'
Table = require 'cli-table'

####################################
endpoints = [
  { method: 'GET', path: 'count_pending_messages'},
  { method: 'GET', path: 'get_messages'},
  { method: 'GET', path: 'get_friends_progress'},
  { method: 'GET', path: 'get_friends_score'},
  # { method: 'POST',path: 'fb_request_messages'}, # << you can add more endpoint here. (with format "/api/users/{user_id}/xxxx" )
  # { method: 'GET', path: 'fb_request_messages'},
  { method: 'POST' },
  { method: 'GET'  }
] 

# log part
L_METHOD  = 3
L_PATH    = 4
L_DYNO    = 7
L_CONNECT = 8
L_SERVICE = 9
####################################
OUT_STR = "/api/users/{user_id}" 

output = (aEndpoints)->
  data = []
  headers = ['URL End Point', 'Called', 'MEAN (average)',  'MEDIAN', 'MODE', 'Dyno']
  aligns  = [null,        'right',  'right', 'right',  'right', null ]

  for v in aEndpoints 
    data.push [
      "#{v.method} #{OUT_STR}#{if v.path then '/' + v.path else ''}", 
      v.count, 
      (if v.mean    then v.mean   else ''), 
      (if v.median  then v.median else ''), 
      (if v.mode    then v.mode   else ''),
      v.dynos
    ]

  table = new Table { head: headers, colAligns: aligns }
  table.push.apply table, data
  console.log table.toString()

line_process = (aLog_line, aEndpoints, aSummary_times)->
  logs = aLog_line.split ' '
  str = logs[L_METHOD].split('=')[1] + " " + logs[L_PATH].split('=')[1]
  
  found_endpoint = false
  for value, index in aEndpoints
    continue if found_endpoint

    unless value.re_pattern
      pattern = "^(#{value.method}\\s/api/users/)(\\d+)" 
      pattern += "(/#{value.path})" if value.path
      pattern += "$"
      value.re_pattern = new RegExp pattern
      value.count = 0
      value.dynos = {}
       
      aSummary_times[index] = []

    if str.match value.re_pattern
      found_endpoint = true 
      value.count += 1

      value.dynos[logs[L_DYNO]] = 0 unless value.dynos[logs[L_DYNO]] 
      value.dynos[logs[L_DYNO]] += 1

      response_time = 0
      for key in [L_CONNECT, L_SERVICE]
        key_value = logs[key].split("=")[1] + ""
        time_value =  parseInt key_value.split("ms")[0]
        response_time += time_value if time_value

      aSummary_times[index].push response_time

main = (aFilename)->
  summary_times = [] 

  rs = fs.createReadStream(aFilename).pipe(es.split()).pipe es.mapSync( 
    (line)->
      rs.pause()
      # process log 
      line_process line, endpoints, summary_times
 
      rs.resume()

  ).on('error', ->
    console.log 'Error while reading file!'

  ).on 'end', ->
    for value, index in endpoints
      time_values = summary_times[index]
      if time_values.length > 0 
        value.mean    = numbers.statistic.mean time_values 
        value.median  = numbers.statistic.median time_values 
        value.mode    = numbers.statistic.mode time_values 

        dyno_sortable = []
        for dyno, count of value.dynos then dyno_sortable.push([dyno, count])

        dyno_sortable.sort (a, b)-> b[1] - a[1]
        value.dynos = (dyno_sortable[0][0] + "").split("=")[1]
      else
        value.dynos = ""

    output endpoints

#################################################################################

filename = process.argv.slice(-1)[0]
 
unless filename == __filename
  file_path = path.join path.dirname(__filename), filename

  fs.exists file_path, (exist)-> 
    if exist
      main file_path
    else
      console.log "File not found! ( #{file_path} )"
else
  console.log "File name invalid!"