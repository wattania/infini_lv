**How to:**

1. Install required package
2. Run with

coffee log_analysis.coffee sample-infini-lv.log

  # OR #

node log_analysis.js sample-infini-lv.log

But I recommended to run with coffeescript.

I love NodeJS with Coffeescript because
"CoffeeScript gives you conveniency like you are driving a car with automatic transmission"
===============================

Tested with MAC OSX Yosemite (10.10.1)
NodeJS v0.10.31

-------------------------------