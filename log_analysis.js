// Generated by CoffeeScript 1.8.0

/*
Author: Wattana Inthaphong
Email: wattaint@gmail.com
 */
var L_CONNECT, L_DYNO, L_METHOD, L_PATH, L_SERVICE, OUT_STR, Table, endpoints, es, file_path, filename, fs, line_process, main, numbers, output, path;

fs = require('fs');

path = require('path');

es = require('event-stream');

numbers = require('numbers');

Table = require('cli-table');

endpoints = [
  {
    method: 'GET',
    path: 'count_pending_messages'
  }, {
    method: 'GET',
    path: 'get_messages'
  }, {
    method: 'GET',
    path: 'get_friends_progress'
  }, {
    method: 'GET',
    path: 'get_friends_score'
  }, {
    method: 'POST'
  }, {
    method: 'GET'
  }
];

L_METHOD = 3;

L_PATH = 4;

L_DYNO = 7;

L_CONNECT = 8;

L_SERVICE = 9;

OUT_STR = "/api/users/{user_id}";

output = function(aEndpoints) {
  var aligns, data, headers, table, v, _i, _len;
  data = [];
  headers = ['URL End Point', 'Called', 'MEAN (average)', 'MEDIAN', 'MODE', 'Dyno'];
  aligns = [null, 'right', 'right', 'right', 'right', null];
  for (_i = 0, _len = aEndpoints.length; _i < _len; _i++) {
    v = aEndpoints[_i];
    data.push(["" + v.method + " " + OUT_STR + (v.path ? '/' + v.path : ''), v.count, (v.mean ? v.mean : ''), (v.median ? v.median : ''), (v.mode ? v.mode : ''), v.dynos]);
  }
  table = new Table({
    head: headers,
    colAligns: aligns
  });
  table.push.apply(table, data);
  return console.log(table.toString());
};

line_process = function(aLog_line, aEndpoints, aSummary_times) {
  var found_endpoint, index, key, key_value, logs, pattern, response_time, str, time_value, value, _i, _j, _len, _len1, _ref, _results;
  logs = aLog_line.split(' ');
  str = logs[L_METHOD].split('=')[1] + " " + logs[L_PATH].split('=')[1];
  found_endpoint = false;
  _results = [];
  for (index = _i = 0, _len = endpoints.length; _i < _len; index = ++_i) {
    value = endpoints[index];
    if (found_endpoint) {
      continue;
    }
    if (!value.re_pattern) {
      pattern = "^(" + value.method + "\\s/api/users/)(\\d+)";
      if (value.path) {
        pattern += "(/" + value.path + ")";
      }
      pattern += "$";
      value.re_pattern = new RegExp(pattern);
      value.count = 0;
      value.dynos = {};
      aSummary_times[index] = [];
    }
    if (str.match(value.re_pattern)) {
      found_endpoint = true;
      value.count += 1;
      if (!value.dynos[logs[L_DYNO]]) {
        value.dynos[logs[L_DYNO]] = 0;
      }
      value.dynos[logs[L_DYNO]] += 1;
      response_time = 0;
      _ref = [L_CONNECT, L_SERVICE];
      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        key = _ref[_j];
        key_value = logs[key].split("=")[1] + "";
        time_value = parseInt(key_value.split("ms")[0]);
        if (time_value) {
          response_time += time_value;
        }
      }
      _results.push(aSummary_times[index].push(response_time));
    } else {
      _results.push(void 0);
    }
  }
  return _results;
};

main = function(aFilename) {
  var rs, summary_times;
  summary_times = [];
  return rs = fs.createReadStream(aFilename).pipe(es.split()).pipe(es.mapSync(function(line) {
    rs.pause();
    line_process(line, endpoints, summary_times);
    return rs.resume();
  }).on('error', function() {
    return console.log('Error while reading file!');
  }).on('end', function() {
    var count, dyno, dyno_sortable, index, time_values, value, _i, _len, _ref;
    for (index = _i = 0, _len = endpoints.length; _i < _len; index = ++_i) {
      value = endpoints[index];
      time_values = summary_times[index];
      if (time_values.length > 0) {
        value.mean = numbers.statistic.mean(time_values);
        value.median = numbers.statistic.median(time_values);
        value.mode = numbers.statistic.mode(time_values);
        dyno_sortable = [];
        _ref = value.dynos;
        for (dyno in _ref) {
          count = _ref[dyno];
          dyno_sortable.push([dyno, count]);
        }
        dyno_sortable.sort(function(a, b) {
          return b[1] - a[1];
        });
        value.dynos = (dyno_sortable[0][0] + "").split("=")[1];
      } else {
        value.dynos = "";
      }
    }
    return output(endpoints);
  }));
};

filename = process.argv.slice(-1)[0];

if (filename !== __filename) {
  file_path = path.join(path.dirname(__filename), filename);
  fs.exists(file_path, function(exist) {
    if (exist) {
      return main(file_path);
    } else {
      return console.log("File not found! ( " + file_path + " )");
    }
  });
} else {
  console.log("File name invalid!");
}
